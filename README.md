# centos

A Docker image based on CentOS including common tools.

Tools:
 - curl
 - jq
 - rsync

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the container image: `make build`
* Try the container: `make shell`
