# centos

ARG CENTOS_VERSION=7


FROM centos:${CENTOS_VERSION} AS centos

ENV \
	container=docker

WORKDIR /

RUN \
	yum install -y \
		epel-release \
	; \
	yum repolist ; \
	yum install -y \
		ca-certificates \
		curl \
		jq \
		rsync \
	; \
	yum clean all ; \
	rpmdb --rebuilddb
